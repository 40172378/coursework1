﻿/*
 * Michal Bohdaniec 40172378
 * invoice class 
 * display invoice based on details from attendee class 
 * last modified : 22/10/2016
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework1
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        public Invoice()
        {
            InitializeComponent();
        }

        //fill the required fields
        public Invoice(string[] invoice) :this()
        {
            textBlock_name.Text = invoice[0] + " " + invoice[1];
            textBlock_institution.Text = invoice[2];
            textBlock_conferencename.Text = invoice[3];

            if (invoice[5] == "True")
                textBlock_totalget.Text = "PAID";
            else
            textBlock_totalget.Text = invoice[4];
        }

        private void btn_close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    
    }
}
