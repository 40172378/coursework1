﻿/*
 * Michal Bohdaniec 40172378
 * certificate class 
 * display certificate based on details of attendee class 
 * last modified : 22/10/2016
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework1
{
    
    public partial class Certificate : Window
    {
        public Certificate()
        {
            InitializeComponent();
        }

        //fill all required fields
        public Certificate(string[] certificate) :this()
        {
            // fill first part of the certificate 
            textBlock_firstsecond.Text = certificate[0] + " " + certificate[1];
            textblock_conference.Text = certificate[2];
            textBlock_Papertitle.Text = certificate[3];
            
            // if attendee had a paper title ie. was a presenter display the rest of information
            if(certificate[3] == "")
            {
                textBlock_presenter1.Text = "";
                textBlock_presenter2.Text = "";
                textBlock_Papertitle.Text = "";
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
