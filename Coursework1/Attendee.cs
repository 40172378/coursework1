﻿/*
 * Michal Bohdaniec 40172378
 * Attendee class extends Person class 
 * used to save Attendee infromation from MainWindow class
 * last modified : 22/10/2016
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    class Attendee : Person
    {
        //private attributes 

        private int attendeeRef;
        
        private string institutionName;
        private string conferenceName;
        private int registrationType; //0 == full || 1 == Student || 2 = Organiser
        private bool paid;
        private bool presenter;
        private string paperTitle;


        //accessors
        public int AttendeeRef
        {
            get { return attendeeRef; }
            // check attendee reference , min 40000 max 60000
            set
            {
                if (value < 40000 && value > 60000)
                    throw new ArgumentException("Fname must not be empty");

                attendeeRef = value;
            }
        }   
        
        public string InstitutionName  
        {
            get { return institutionName; }
            // may be left blank
            set { institutionName = value; }    
        }  
        public string ConferenceName
        {
            get { return conferenceName; }
            // check if field not empty
            set
            {
                if (value == "" || value == " ")
                    throw new ArgumentException("name must not be empty");
                conferenceName = value;
            }
        }   
        public int RegistrationType  
        {
            get { return registrationType; }
            //only three options in the combo box. registration type will be saved as an int. 
            set
            {
                if(value != 0 && value != 1 && value != 2)
                    throw new ArgumentException("Combo box item must be selected");
                registrationType = value;
            }
        }  
        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }  
        public bool Presenter
        {
            get { return presenter; }
            set { presenter = value; }
        }   
        public string PaperTitle   
        {
            get { return paperTitle; }
            //check presenter using AND logic. Both presenter boolean and paper title must be true 
            set
            {
                if (presenter == true && value == "")
                    throw new ArgumentException("Sname must not be empty");
                if (presenter == false && value != "" ) 
                    throw new ArgumentException("Sname must not be empty");

                paperTitle = value;
            }
        }
        //check the registration type 
        public int Get_Cost()     
        {
          
            //Check if student
            if (registrationType == 1)
                return 300;
            //Check if organiser
            else if (registrationType == 2)
                return 0;
            
            //if both = false then return full price.
            else return 500;

        }
    }
}
