﻿///By Michal Bohdaniec 40172378 
///coursework 1 
///GUI Interface
///

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coursework1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Attendee Chujek = new Attendee();


        private void setVal(Attendee Att)
        {
            //check if all text_boxes are filled       
            if (textBox_firstName != null &&
                textBox_secondName != null &&
                textBox_attendeeRef != null &&  // TODO : Change string to ushort 
                textBox_institution != null &&
                textBox_conference != null &&
                textBox_paperTitle != null
                )

                //check if anything from combo box is selected
                if (comboBox_regType.SelectedIndex > -1)
                {
                    //both gates passed , fill the attendee class 
                    Att.FirstName = textBox_firstName.Text;
                    Att.SecondName = textBox_secondName.Text;
                    Att.AttendeeRef = textBox_attendeeRef.Text;
                    Att.InstitutionName = textBox_institution.Text;
                    Att.ConferenceName = textBox_conference.Text;
                    Att.PaperTitle = textBox_paperTitle.Text;

                    //Check Bool paid and presenter 
                    if (checkBox_paid.IsChecked == true) Att.Paid = true;
                    if (checkBox_presenter.IsChecked == true) Att.Presenter = true;

                    //fill in the registration type
                    Att.RegistrationType = comboBox_regType.Text;

                }
                else throw new ArgumentException("Boxes not filled");

        }
        public MainWindow()
        {

            InitializeComponent();

        }
        //SET  - STATE = NOT COMPLETED 
        private void button_set_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                setVal(Chujek);
            }
            catch (Exception excep)
            {
                System.Windows.MessageBox.Show("Error: Please fill in all fields");
            }
        }

        private void comboBox_regType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        //CLEAR      STATE = NOT COMPLETED 
        private void button_clear_Click(object sender, RoutedEventArgs e)
        {
            textBox_firstName.Clear();
            textBox_secondName.Clear();
            textBox_attendeeRef.Clear();
            textBox_institution.Clear();
            textBox_conference.Clear();
            textBox_paperTitle.Clear();
            //TODO : CLEAR CHECK BOXES 
            //TODO : CLEAR COMBOBOX                                                      

        }

        private void button_get_Click(object sender, RoutedEventArgs e)
        {
            if(Chujek.AttendeeRef == null)

            textBox_firstName.Text = Chujek.FirstName;
            textBox_secondName.Text = Chujek.SecondName;
            textBox_attendeeRef.Text = Chujek.AttendeeRef;
            textBox_conference.Text = Chujek.ConferenceName;
            textBox_paperTitle.Text = Chujek.PaperTitle;

            //TODO : GET CHECK BOXES AND COMBOBOX
        }
    }
}
