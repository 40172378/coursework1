﻿/*
 * Michal Bohdaniec 40172378   
 * Person class - Stores information name 
 * is a base class of attendee 
 * last modified : 22/10/2016
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    class Person
    {
        
        private string firstName;
        private string secondName;

        //Get/set methods 
        public string FirstName
        {
            get { return firstName; }
            //check if input is not empty. 
            set        
            {   
                if (value == "" || value == " ")
                    throw new ArgumentException("name must not be empty");
                firstName = value;
            }
        }  
        public string SecondName
        {
            get { return secondName; }
            //check if input is not empty. 
            set
            {
                if (value == "" || value == " ")
                    throw new ArgumentException("name must not be empty");
                secondName = value;
            }
        }  
    }
}
