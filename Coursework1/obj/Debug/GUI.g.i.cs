﻿#pragma checksum "..\..\GUI.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "39BAE376F8AFE60675A6F1DE5C05CE8A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Coursework1;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Coursework1 {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock_name;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock_Attendee;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock_Institution;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock_Conference;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock_Registration;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock_paid;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock_Presenter;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock_Paper;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox_firstName;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox_attendeeRef;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox_institution;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox_conference;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox_paperTitle;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox_secondName;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBox_regType;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkBox_paid;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkBox_presenter;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button_set;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button_clear;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button_get;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button_invoice;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\GUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button_certificate;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Coursework1;component/gui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\GUI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.textBlock_name = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.textBlock_Attendee = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.textBlock_Institution = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.textBlock_Conference = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.textBlock_Registration = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.textBlock_paid = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.textBlock_Presenter = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.textBlock_Paper = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.textBox_firstName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.textBox_attendeeRef = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.textBox_institution = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.textBox_conference = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.textBox_paperTitle = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.textBox_secondName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.comboBox_regType = ((System.Windows.Controls.ComboBox)(target));
            
            #line 36 "..\..\GUI.xaml"
            this.comboBox_regType.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBox_regType_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.checkBox_paid = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 17:
            this.checkBox_presenter = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 18:
            this.button_set = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\GUI.xaml"
            this.button_set.Click += new System.Windows.RoutedEventHandler(this.button_set_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.button_clear = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\GUI.xaml"
            this.button_clear.Click += new System.Windows.RoutedEventHandler(this.button_clear_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.button_get = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\GUI.xaml"
            this.button_get.Click += new System.Windows.RoutedEventHandler(this.button_get_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.button_invoice = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\GUI.xaml"
            this.button_invoice.Click += new System.Windows.RoutedEventHandler(this.button_invoice_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.button_certificate = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

