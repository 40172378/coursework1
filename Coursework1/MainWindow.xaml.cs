﻿/*
 * Michal Bohdaniec 40172378
 * MainWindow class
 * Uses Attendee class to set and display information 
 * last modified : 22/10/2016
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coursework1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //create an instance of the Attendee class. 
        Attendee A1 = new Attendee();

        public MainWindow()
        {

            InitializeComponent();

        }
        //Set button implementation
        private void button_set_Click(object sender, RoutedEventArgs e)
        {
            //Begin filling the Attendee fields with the fields from the GUI.
            //Fill check-boxes and institution name because no validation required.
            //When exception is cought , cleardata() method is activated.
            if (checkBox_paid.IsChecked == true)       
                A1.Paid = true;
            else A1.Paid = false;

            if (checkBox_presenter.IsChecked == true)
                A1.Presenter = true;
            else A1.Presenter = false;

            A1.InstitutionName = textBox_institution.Text;

            //Fill fields that cannot be left blank.
            try
            {
                A1.FirstName = textBox_firstName.Text;
                A1.SecondName = textBox_secondName.Text;
                A1.ConferenceName = textBox_conference.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show("Some information left blank");
                cleardata();
                return;
            }

            //Fill Attendee reference number.(40000 - 60000 range)
            try
            {
                A1.AttendeeRef = Convert.ToInt32(textBox_attendeeRef.Text);
            }
            catch (Exception excep)
            {
                MessageBox.Show("Invalid attendee reference number");
                cleardata();
                return;
            }

            //Fill Paper title field.
            try
            {
                A1.PaperTitle = textBox_paperTitle.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show("Please fill paper title field only if you are a presenter");
                cleardata();
                return;
            }

            //Set Registration type
            try
            {
                A1.RegistrationType = comboBox_regType.SelectedIndex;
            }
            catch (Exception excep)
            {
                MessageBox.Show("Registration Type box must not be empty");
                cleardata();
                return;
            }
        }

        private void comboBox_regType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        
        //Clear button
        private void button_clear_Click(object sender, RoutedEventArgs e)
        {
            //Activate the cleardata function.
            cleardata();
        }

        //Get button
        private void button_get_Click(object sender, RoutedEventArgs e)
        {
            //All validation has been completed, assuming all data is correct.
            textBox_firstName.Text = A1.FirstName;
            textBox_secondName.Text = A1.SecondName;
            textBox_attendeeRef.Text = A1.AttendeeRef.ToString();
            textBox_institution.Text = A1.InstitutionName;
            textBox_conference.Text = A1.ConferenceName;
            textBox_paperTitle.Text = A1.PaperTitle;
            //determine the state of check boxes
            if (A1.Paid == true)
                checkBox_paid.IsChecked = true;
            else checkBox_paid.IsChecked = false;

            if (A1.Presenter == true)
                checkBox_presenter.IsChecked = true;
            else checkBox_presenter.IsChecked = false;
            //combo box information saved as an int, therefore the number in the Attendee instance corresponds 
            //to the combo box index. 
            comboBox_regType.SelectedIndex = A1.RegistrationType; 

           
        }

        //Invoice button
        private void button_invoice_Click(object sender, RoutedEventArgs e)
        {
            //Check if attendee is filled with data. if it is, the invoice can be constructed
            if (A1.FirstName != "")
            {
                //create string array with infromation needed to display invoice  
                string[] invoicepack = new string[] { A1.FirstName, A1.SecondName, A1.InstitutionName, A1.ConferenceName, A1.Get_Cost().ToString(), A1.Paid.ToString() };
                Invoice i1 = new Invoice(invoicepack);
                i1.ShowDialog();
            }

            else MessageBox.Show("Error: Database empty");

        }

        //Certificate Button
        private void button_certificate_Click(object sender, RoutedEventArgs e)
        {
            //Check if attendee is filled with data. if it is, the certificate can be constructed
            if (A1.FirstName != "")
            {
                //create string array with infromation needed to display certificate
                string[] certificatepack = new string[] { A1.FirstName, A1.SecondName, A1.ConferenceName, A1.PaperTitle };
                Certificate c1 = new Certificate(certificatepack);
                c1.ShowDialog();
            }

            else MessageBox.Show("Error: Database empty");
        }

        //Function used to clear data when an exception is thrown or when "Clear" button is pressed.
        private void cleardata()    
         {
            
            textBox_firstName.Clear();
            textBox_secondName.Clear();
            textBox_attendeeRef.Clear();
            textBox_institution.Clear();
            textBox_conference.Clear();
            textBox_paperTitle.Clear();


            checkBox_paid.IsChecked = false;
            checkBox_presenter.IsChecked = false;

            comboBox_regType.SelectedIndex = -1;

         }
        
    }





}
